# README #

### Cubical Marching Squares (CMS) Isosurface Extraction C++ Opensource Implementation ###

Version 1.0

### What is this? ###
This project was started as the final project for my [Masters Thesis](https://grassovsky.wordpress.com/2014/09/09/cubical-marching-squares-implementation/).

It is based on this original paper: http://graphics.csie.ntu.edu.tw/CMS/

This project is a loose, partial implementation of the technique in the white paper linked above.
  - Loose, because all algorithms within the original technique described in the paper, have not been followed strictly.
  - Partial, because as of yet, all of the technique hasn't been implemented fully, thus it lacks some of the features described in the original paper 

### The code? ###
Currently its a mixture of C++98 and C++11 (and even some C stuff :-/) This will all soon have to be homogenised to standard C++11. No 3rd party libs are used thus there are no external dependencies. STL is used throughout. Any 3rd party code is referenced accordingly in the source/header files. 

Please note that this is still very much a work in progress and there is lots of things that could/will be improved, so watch out for updates. If you find sublime bugs in the code or things which could be improved and don't seem to have a //todo comment somewhere around them, feel free to let me know. Would greatly appreciate all feedback.

This is _still_ a partial implementation, meaning that it doesn't actually preserve sharp features or provide 2D and 3D disambiguation. Currently it produces meshes with a Marching-Cubes-like topology, but at an adaptive resolution.


### The licence? ###
This project is licensed under the [BSD 3-clause licence](http://opensource.org/licenses/BSD-3-Clause). For more information refer to the LICENCE.md file in the root directory of the project.


### How do I get set up? ###

The project contains a Qt .pro file which could be opened with the [opensource qtcreator](http://www.qt.io/download-open-source/) (version > 3.0 is recommended).

In it there are a few lines at the top which configure it to compile as a console application or a static library. Uncomment accordingly.

If you choose the console app (on by default) it will run from main.cpp. In main.cpp there is an example usage of the interface with examples of different implicit functions which could be meshed. (All output will be found under output/)

If you choose to build a static library the project will output a .a static lib (in output/) upon compilation. This library can then be statically linked to any project and then used as suggested in main.cpp.  

### What was it tested on? ###
Ubuntu 12.04 x86
Qt 5.2.0 (GCC 4.6.1, 32 bit)

OSX 10.10.3 (Yosemite)
Qt 5.5.0 (Clang 6.0 (Apple), 64 bit)

### Contacts? ###
For any issues/bugs/requests/recommendations please send me an email to goro.rassovsky@gmail.com (George Rassovsky)