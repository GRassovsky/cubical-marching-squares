//////////////////////////////////////////////
/// @file main.cpp
/// @brief example usage of the CMS algorithm
///
/// The Cubical Marching Squares (CMS) Project
/// licensed under the 3 clause BSD licence
/// found in LICENCE.md 2015
///
/// @author George Rassovsky
/// (goro.rassovsky@gmail.com)
//////////////////////////////////////////////

#include <iostream>
#include <vector>
#include <cmath>
#include "isosurface_t.h"
#include "algcms.h"
#include "vec3.h"
#include "mesh.h"



class ExampleClass
{
  // Note that users don't need this extra level of indirection which is introduced
  // here by operator() calling another function... this extra call will slow the
  // sampling process (theoretically). The user can have operator() return the function
  // straight away.
  //
  // This extra level of indirection is introduced just to ease the accomodation of
  // the examples of implicit functions which could be meshed using the algorithm.

  /** Sphere function **/
  float sphereFunction(float x, float y, float z) const
  {
    return (x*x+y*y+z*z)-1.f;
  }

  /** Cube function **/
  float cubeFunction(float x, float y, float z) const
  {
    return (std::max(std::fabs(x)-1.f, std::max((std::fabs(y)-1.f), std::fabs(z)-1.f)));
  }

  /** Cone function **/
  float coneFunction(float x, float y, float z) const
  {
    return ( (((x*x)+(y*y))/0.1f) - ((z-1.f)*(z-1.f)));
  }

  /** Anti-tank-like function **/
  float antiTankFunction(float x, float y, float z) const
  {
    return ((x*x)*(y*y) + (x*x)*(z*z) + (y*y)*(z*z) - 0.01f);
  }

  /** Heart function **/
  float heartFunction(float x, float y, float z) const
  {
      return ( std::pow((x*x + y*y + 2.f*(z*z)-0.5f), 3.0) - y*y*y * (-0.01f*z*z+x*x) );
  }

  /** Torus function **/
  float torusFunction(float x, float y, float z) const
  {
    float R = 0.45f; float r = 0.2f; float x0 = x-0.25f;
    return ( ((x0*x0+y*y+z*z+R*R-r*r)*(x0*x0+y*y+z*z+R*R-r*r)) - (4.f*R*R)*(z*z+x0*x0) );
  }

  /** Double torus function **/
  float doubleTorusFunction(float x, float y, float z) const
  {
    return -(0.01f-x*x*x*x + 2.f*x*x*x*x*x*x - x*x*x*x*x*x*x*x + 2.f*x*x*y*y - 2.f*x*x*x*x*y*y - y*y*y*y - z*z);
  }

  /** Interlinked torii function **/
  float linkedToriiFunction(float x, float y, float z) const
  {
    float R = 0.45f; float r = 0.2f; float x0 = x-0.25f; float x1 = x+0.25f;
    return ( ( ((x0*x0+y*y+z*z+R*R-r*r)*(x0*x0+y*y+z*z+R*R-r*r)) - (4.f*R*R)*(z*z+x0*x0) ) *
             (((x1*x1+y*y+z*z+R*R-r*r)*(x1*x1+y*y+z*z+R*R-r*r)) - (4.f*R*R)*(x1*x1+y*y)) );
  }

public:

  /** The overloaded function call operator **/
  float operator()(float x, float y, float z) const
  {
    return torusFunction(x,y,z);
  }

};


//---------------------------------------------------


/// @todo add limits and checks to the user values

// User settings
static float BBOX_SIZE                  = 2.f;
static int MIN_OCTREE_RES               = 2;
static int MAX_OCTREE_RES               = 8;
static float COMPLEX_SURFACE_THRESHOLD  = 0.85f;
static const char * OBJ_NAME            = "test.obj";

int ADDRESS_SIZE = MAX_OCTREE_RES; // To be used by some of the classes

int main()
{
  ExampleClass t;

  cms::Isosurface_t<ExampleClass> iso(&t);

  float halfSize = (BBOX_SIZE/2.f);

  cms::Range container[3] = {
    cms::Range(-halfSize,halfSize),
    cms::Range(-halfSize,halfSize),
    cms::Range(-halfSize,halfSize) };

  cms::AlgCMS cmsAlg(&iso, container, MIN_OCTREE_RES, MAX_OCTREE_RES);

  cms::Mesh mesh;

  // Set the complex surface threshold
  cmsAlg.setComplexSurfThresh(COMPLEX_SURFACE_THRESHOLD);

  // Proceed to extract the surface <runs the algorithm>
  cmsAlg.extractSurface(mesh);

  // (OPTIONAL) Export the generated octree in the form of a maya script
//  cmsAlg.exportOctreeToMaya("octree.py", true);

  // (OPTIONAL) Export the generated octree in the form of a blender script
//  cmsAlg.exportOctreeToBlender("octree.py", true);

  // Export the created mesh as an .OBJ file
  mesh.exportOBJ(OBJ_NAME);

  return EXIT_SUCCESS;
}
