//////////////////////////////////////////////
/// @file util.cpp
/// @brief utility functions implementation
///
/// The Cubical Marching Squares (CMS) Project
/// licensed under the 3 clause BSD licence
/// found in LICENCE.md 2015
///
/// @author George Rassovsky
/// (goro.rassovsky@gmail.com)
//////////////////////////////////////////////

#include "util.h"
#include <cstdio>

namespace cms
{

int util::getPowerOfTwo(uint exponent)
{
  // A left-bitshift of 1 acts like a power of two function for the given exponent:
  // eg 1 << 6 == pow(2, 6) == 64

  return (1 << exponent);
}

//-----------------------------------------------

bool util::isPowerOfTwo(uint x)
{
  // First, check that x is not 0.
  // Then, bitwise AND x and x-1 which would amount to 0 if x is a power-of-2 number:
  // eg 0b100 (4) & 0b011 (3) = b000

  return (x && !(x & (x-1)));
}

//-----------------------------------------------

void util::printTime(int totalSeconds, const char* message)
{
  int ss = totalSeconds % 60;
  int mm = (totalSeconds / 60) % 60;
  int hh = (totalSeconds / 60) / 60;

  printf("\n%s \n%02i:%02i:%02i [hh:mm:ss] \n\n", message, hh, mm, ss);
}

} //namespace cms
