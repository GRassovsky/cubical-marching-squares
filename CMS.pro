TARGET = CMS
OBJECTS_DIR=obj


# To compile a console app configured from main
# uncomment below:
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt


# To compile a static library (.a)
# uncomment below:
#TEMPLATE = lib
#CONFIG += staticlib


# Define a debug macro based on the build
CONFIG(debug, debug|release) {
DEFINES += _DEBUG
}


# Turn on C++11 #todo this is not working properly with clang
QMAKE_CXXFLAGS += -std=c++11 -Wall -Wextra -pedantic


# All the source files
SOURCES += \
        src/main.cpp \
        src/vec3.cpp \
        src/isosurface.cpp \
        src/mesh.cpp \
        src/point.cpp \
        src/edge.cpp \
        src/strip.cpp \
        src/cell.cpp \
        src/array3d.cpp \
        src/index3d.cpp \
        src/octree.cpp \
        src/util.cpp \
        src/vertex.cpp \
        src/algcms.cpp \
        src/address.cpp


# All the header files
HEADERS += \
        include/configs.h \
        include/vec3.h \
        include/isosurface.h \
        include/isosurface_t.h \
        include/mesh.h \
        include/tables.h \
        include/range.h \
        include/point.h \
        include/edge.h \
        include/strip.h \
        include/cell.h \
        include/face.h \
        include/array3d.h \
        include/index3d.h \
        include/octree.h \
        include/util.h \
        include/types.h \
        include/vertex.h \
        include/algcms.h \
        include/address.h \

INCLUDEPATH = ./include
